import com.betse.betraining.spring.helloworld2.*;
import junit.framework.*;
import org.junit.Test;
import org.junit.runner.*;
import org.springframework.beans.factory.annotation.*;
import org.springframework.test.context.*;
import org.springframework.test.context.junit4.*;

/**
 * Created by IntelliJ IDEA.
 * User: angelo
 * Date: 20/04/17
 * Time: 12.09
 * SVN Id: $Id:$
 * .
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {ContextConfigurator.class})
public class HelloWorldTest extends TestCase {

  @Autowired
  private GreetExecutor greetExecutor;


  @Test
  public void testInjection() {
    assertNotNull("Executor not injected",greetExecutor);

  }

  @Test
  public void executeGreeting() {
    greetExecutor.greetTheWorld();
    
  }

}
