package com.betse.betraining.spring.helloworld2;

import org.springframework.context.*;
import org.springframework.context.annotation.*;

/**
 * Created by IntelliJ IDEA.
 * User: angelo
 * Date: 20/04/17
 * Time: 10.49
 * SVN Id: $Id:$
 * .
 */
public class RunMe {
  public static void main(String[] args) {

    ApplicationContext context = new AnnotationConfigApplicationContext(ContextConfigurator.class);

    GreetExecutor executor = (GreetExecutor) context.getBean(GreetExecutor.class);

    executor.greetTheWorld();

  }
}
