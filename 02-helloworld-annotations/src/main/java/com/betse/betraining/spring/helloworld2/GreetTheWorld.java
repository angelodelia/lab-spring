package com.betse.betraining.spring.helloworld2;

/**
 * Created by IntelliJ IDEA.
 * User: angelo
 * Date: 20/04/17
 * Time: 10.30
 * SVN Id: $Id:$
 * .
 */
public interface GreetTheWorld {

  public String sayHello();
}
