package com.betse.betraining.spring.helloworld2;

import com.betse.betraining.spring.helloworld2.impl.*;
import org.springframework.beans.factory.annotation.*;
import org.springframework.context.annotation.*;

/**
 * Created by IntelliJ IDEA.
 * User: angelo
 * Date: 20/04/17
 * Time: 11.46
 * SVN Id: $Id:$
 * .
 */
@Configuration
@ComponentScan(basePackages = {
  "com.betse.betraining.spring.helloworld2"
})
public class ContextConfigurator {

  @Bean
  @Qualifier("it")
  GreetTheWorld getGreeter_it() {
    return new GreetTheWorldInItaliano();
  }

  @Bean
  @Qualifier("pl")
  GreetTheWorld getGreeter_pl() {
    return new GreetTheWorldPoPolsku();
  }

  @Bean
  @Qualifier("en")
  GreetTheWorld getGreeter_en() {
    return new GreetTheWorldInEnglish();
  }

  @Bean
  GreetExecutor getExecutor() {
    return new GreetExecutor();
  }

}
