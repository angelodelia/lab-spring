import com.betse.betrained.spring.airportdistances.conf.*;
import com.betse.betrained.spring.airportdistances.dao.*;
import com.betse.betrained.spring.airportdistances.vo.*;
import junit.framework.*;
import org.junit.Test;
import org.junit.runner.*;
import org.springframework.beans.factory.annotation.*;
import org.springframework.test.context.*;
import org.springframework.test.context.junit4.*;

import java.util.*;

/**
 * Created by IntelliJ IDEA.
 * User: angelo
 * Date: 24/04/17
 * Time: 15.55
 * SVN Id: $Id:$
 * .
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {ContextConfigurer.class})

public class AirportDaoTest extends TestCase {


  @Autowired
  private AirportDAO airportDAO;

  @Test
  public void testDAO() {
    assertNotNull("Injected Service is null!!",airportDAO);

    Map<String, Airport> iataAirports = airportDAO.allIataAirports();
    assertNotNull("Iata Airport list is null",iataAirports);
    assertTrue("NO Iata Airports found",iataAirports.size()>0);

    Airport chopina = airportDAO.getAirportByIataCode("WAW");
    assertNotNull("Chopin airport not found",chopina);
    assertEquals("Chopin airport elevetion",chopina.getElevation().doubleValue(),362.0,1e-2);

  }



}
