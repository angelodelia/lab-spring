import com.betse.betrained.spring.airportdistances.conf.*;
import com.betse.betrained.spring.airportdistances.services.*;
import junit.framework.*;
import org.junit.runner.*;
import org.springframework.beans.factory.annotation.*;
import org.springframework.test.context.*;
import org.springframework.test.context.junit4.*;

/**
 * Created by IntelliJ IDEA.
 * User: angelo
 * Date: 24/04/17
 * Time: 17.31
 * SVN Id: $Id:$
 * .
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {ContextConfigurer.class})

public class AirportDistanceSerivceTest extends TestCase {
  @Autowired
  private AirportDistancesService airportDistancesService;

  @org.junit.Test
  public void testDistance() {
    assertNotNull("Service is null",airportDistancesService);

    double distance = airportDistancesService.getDistance("WAW","WAW");
    assertEquals("Distance from airport to itself",0.0,distance);

    distance = airportDistancesService.getDistance("WAW","FCO");
    assertEquals("Distance from WAW to FCO", 1326185.55, distance,1e-2);

    distance = airportDistancesService.getDistance("WAW","WMI");
    assertEquals("Distance from WAW to WMI", 38296.32, distance,1e-2);


  }
}
