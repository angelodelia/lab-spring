package com.betse.betrained.spring.airportdistances.services.impl;

import com.betse.betrained.spring.airportdistances.dao.*;
import com.betse.betrained.spring.airportdistances.services.*;
import com.betse.betrained.spring.airportdistances.vo.*;
import org.slf4j.*;
import org.springframework.beans.factory.annotation.*;
import org.springframework.stereotype.*;

import java.util.*;

/**
 * Created by IntelliJ IDEA.
 * User: angelo
 * Date: 24/04/17
 * Time: 15.38
 * SVN Id: $Id:$
 * .
 */
@Service("AirportDistanceService")
public class AirportDistancesServicesImpl implements AirportDistancesService {
  private static final Logger log = LoggerFactory.getLogger(AirportDistancesServicesImpl.class);

  private final AirportDAO airportDAO;


  @Autowired
  public AirportDistancesServicesImpl(AirportDAO airportDAO) {
    this.airportDAO = airportDAO;

  }


  public List<Airport> allIataAirports() {
    Map<String,Airport> all = airportDAO.allIataAirports();

    Collection vals = all.values();
    if (vals instanceof List) {
      return (List<Airport>)vals;
    } else {
      return new ArrayList<Airport>(vals);
    }

  }

  public Double getDistance(String iataCode1, String iataCode2) {
    log.debug("getDistance entry, {},{}",iataCode1,iataCode2);

    Airport from = airportDAO.getAirportByIataCode(iataCode1);
    Airport to = airportDAO.getAirportByIataCode(iataCode2);
    if (from == null || to == null) {
      throw new IllegalArgumentException("One of the 2 airports cannot be found!!");

    }

    double distance = distance(from.getLatitude().doubleValue(),to.getLatitude().doubleValue(),
                               from.getLongitude().doubleValue(),to.getLongitude().doubleValue(),
                                from.getElevation().doubleValue(),to.getElevation().doubleValue()) ;

    log.debug("distance : {}", distance);


    log.debug("getDistance exit");
    return distance;
  }

  /**
   * I FOUND IT ON INTERNET, DO NOT ASK ME MORE!!
   *
   * Calculate distance between two points in latitude and longitude taking
   * into account height difference. If you are not interested in height
   * difference pass 0.0. Uses Haversine method as its base.
   *
   * lat1, lon1 Start point lat2, lon2 End point el1 Start altitude in meters
   * el2 End altitude in meters
   * @returns Distance in Meters
   */
  public double distance(double lat1, double lat2, double lon1,
                                double lon2, double el1, double el2) {

    final int R = 6371; // Radius of the earth

    double latDistance = Math.toRadians(lat2 - lat1);
    double lonDistance = Math.toRadians(lon2 - lon1);
    double a = Math.sin(latDistance / 2) * Math.sin(latDistance / 2)
      + Math.cos(Math.toRadians(lat1)) * Math.cos(Math.toRadians(lat2))
      * Math.sin(lonDistance / 2) * Math.sin(lonDistance / 2);
    double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
    double distance = R * c * 1000; // convert to meters

    double height = el1 - el2;

    distance = Math.pow(distance, 2) + Math.pow(height, 2);

    return Math.sqrt(distance);
  }
}
