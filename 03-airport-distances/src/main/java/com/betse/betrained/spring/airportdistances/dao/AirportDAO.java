package com.betse.betrained.spring.airportdistances.dao;

import com.betse.betrained.spring.airportdistances.vo.*;

import java.util.*;

/**
 * Created by IntelliJ IDEA.
 * User: angelo
 * Date: 24/04/17
 * Time: 15.31
 * SVN Id: $Id:$
 * .
 */

public interface AirportDAO {

  public Map<String,Airport> allIataAirports();

  Airport getAirportByIataCode(String iataCode);
}
