package com.betse.betrained.spring.airportdistances.dao.impl;

import com.betse.betrained.spring.airportdistances.dao.*;
import com.betse.betrained.spring.airportdistances.vo.*;
import net.sf.jsefa.*;
import org.springframework.beans.factory.annotation.*;
import org.springframework.stereotype.*;

import javax.annotation.*;
import java.io.*;
import java.util.*;

/**
 * Created by IntelliJ IDEA.
 * User: angelo
 * Date: 24/04/17
 * Time: 15.33
 * SVN Id: $Id:$
 * .
 */
@Component
public class AirportDAOJSEFAImpl implements AirportDAO {
  private static final org.slf4j.Logger log = org.slf4j.LoggerFactory.getLogger(AirportDAOJSEFAImpl.class);

  private Map<String,Airport> allAirports;
  private Map<String,Airport> iataAirports;

  private final Deserializer deserializer;

  @Autowired
  public AirportDAOJSEFAImpl(Deserializer deserializer) {
    this.deserializer = deserializer;
  }

  @PostConstruct
  private void loadData() {
    log.debug("loadData entry");

    Reader reader = null;
    try {
      reader = new InputStreamReader(this.getClass().getClassLoader().getResourceAsStream("airport-codes.csv"));
      deserializer.open(reader);
      allAirports = new HashMap<String, Airport>();
      iataAirports = new HashMap<String, Airport>();
      while (deserializer.hasNext()) {
        Airport airport = deserializer.next();
        allAirports.put(airport.getId(),airport);
        if (airport.getIataCode()!=null) {
          iataAirports.put(airport.getIataCode(),airport);
        }


      }
      deserializer.close(true);
    } catch ( Exception e) {
      log.error("Exception reading",e);
    }

    log.debug("loadData exit");


  }

  public Map<String,Airport> allIataAirports() {

      return iataAirports;
  }

  public Airport getAirportByIataCode(String iataCode) {
    return iataAirports.get(iataCode);
  }
}
