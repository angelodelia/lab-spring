package com.betse.betrained.spring.airportdistances.vo;

import net.sf.jsefa.csv.annotation.*;

import java.math.*;

/**
 * Created by IntelliJ IDEA.
 * User: angelo
 * Date: 24/04/17
 * Time: 15.20
 * SVN Id: $Id:$
 * .
 */
@CsvDataType()
public class Airport {

//  ident,
  @CsvField(pos = 1)
  private String id;
// type,
@CsvField(pos = 2)
  private String type;
// name,
@CsvField(pos = 3)
  private String name;
// latitude_deg,
@CsvField(pos = 4)
  private BigDecimal latitude;
// longitude_deg,
@CsvField(pos = 5)
  private BigDecimal longitude;
// elevation_ft,
@CsvField(pos = 6)
  private BigDecimal elevation;
// continent,
@CsvField(pos = 7)
  private String continent;
// iso_country,
@CsvField(pos = 8)
  private String isoCountry;
// iso_region,
@CsvField(pos = 9)
  private String isoRegion;
// municipality,
@CsvField(pos = 10)
  private String municipality;
// gps_code,
@CsvField(pos = 11)
  private String gpsCode;
// iata_code,
@CsvField(pos = 12)
  private String iataCode;
// local_code
@CsvField(pos = 13)
  private String localCode;


  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getType() {
    return type;
  }

  public void setType(String type) {
    this.type = type;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public BigDecimal getLatitude() {
    return latitude;
  }

  public void setLatitude(BigDecimal latitude) {
    this.latitude = latitude;
  }

  public BigDecimal getLongitude() {
    return longitude;
  }

  public void setLongitude(BigDecimal longitude) {
    this.longitude = longitude;
  }

  public BigDecimal getElevation() {
    return elevation;
  }

  public void setElevation(BigDecimal elevation) {
    this.elevation = elevation;
  }

  public String getContinent() {
    return continent;
  }

  public void setContinent(String continent) {
    this.continent = continent;
  }

  public String getIsoCountry() {
    return isoCountry;
  }

  public void setIsoCountry(String isoCountry) {
    this.isoCountry = isoCountry;
  }

  public String getIsoRegion() {
    return isoRegion;
  }

  public void setIsoRegion(String isoRegion) {
    this.isoRegion = isoRegion;
  }

  public String getMunicipality() {
    return municipality;
  }

  public void setMunicipality(String municipality) {
    this.municipality = municipality;
  }

  public String getGpsCode() {
    return gpsCode;
  }

  public void setGpsCode(String gpsCode) {
    this.gpsCode = gpsCode;
  }

  public String getIataCode() {
    return iataCode;
  }

  public void setIataCode(String iataCode) {
    this.iataCode = iataCode;
  }

  public String getLocalCode() {
    return localCode;
  }

  public void setLocalCode(String localCode) {
    this.localCode = localCode;
  }

  @Override
  public String toString() {
    final StringBuilder sb = new StringBuilder("Airport{");
    sb.append("id='").append(id).append('\'');
    sb.append(", type='").append(type).append('\'');
    sb.append(", name='").append(name).append('\'');
    sb.append(", latitude=").append(latitude);
    sb.append(", longitude=").append(longitude);
    sb.append(", elevation=").append(elevation);
    sb.append(", continent='").append(continent).append('\'');
    sb.append(", isoCountry='").append(isoCountry).append('\'');
    sb.append(", isoRegion='").append(isoRegion).append('\'');
    sb.append(", municipality='").append(municipality).append('\'');
    sb.append(", gpsCode='").append(gpsCode).append('\'');
    sb.append(", iataCode='").append(iataCode).append('\'');
    sb.append(", localCode='").append(localCode).append('\'');
    sb.append('}');
    return sb.toString();
  }
}
