package com.betse.betrained.spring.airportdistances.services;

import com.betse.betrained.spring.airportdistances.vo.*;

import java.util.*;

/**
 * Created by IntelliJ IDEA.
 * User: angelo
 * Date: 24/04/17
 * Time: 15.37
 * SVN Id: $Id:$
 * .
 */
public interface AirportDistancesService {


  List<Airport> allIataAirports();

  Double getDistance(String iataCode1, String iataCode2);

}
