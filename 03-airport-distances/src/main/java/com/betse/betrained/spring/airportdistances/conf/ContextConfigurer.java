package com.betse.betrained.spring.airportdistances.conf;

import com.betse.betrained.spring.airportdistances.vo.*;
import net.sf.jsefa.*;
import net.sf.jsefa.common.lowlevel.filter.*;
import net.sf.jsefa.csv.*;
import net.sf.jsefa.csv.config.*;
import org.springframework.context.annotation.*;

/**
 * Created by IntelliJ IDEA.
 * User: angelo
 * Date: 24/04/17
 * Time: 15.33
 * SVN Id: $Id:$
 * .
 */
@Configuration
@ComponentScan(basePackages = {
  "com.betse.betrained.spring.airportdistances.dao",
  "com.betse.betrained.spring.airportdistances.services"

})
public class ContextConfigurer {

  @Bean
  public Deserializer createDeserializer() {
    CsvConfiguration configuration = new CsvConfiguration();
    configuration.setFieldDelimiter(',');
    configuration.setLineFilter(new HeaderAndFooterFilter(1,false,false));
    Deserializer deserializer = CsvIOFactory.createFactory(configuration, Airport.class).createDeserializer();
    return deserializer;
  }


}
