package com.betse.betraining.spring.helloworld;

import org.springframework.context.*;
import org.springframework.context.support.*;

/**
 * Created by IntelliJ IDEA.
 * User: angelo
 * Date: 20/04/17
 * Time: 10.49
 * SVN Id: $Id:$
 * .
 */
public class RunMe {
  public static void main(String[] args) {

    ApplicationContext context = new ClassPathXmlApplicationContext("myfirstcontext.xml");
    GreetExecutor executor = (GreetExecutor) context.getBean("greetingExecutor");

    executor.greetTheWorld();

  }
}
