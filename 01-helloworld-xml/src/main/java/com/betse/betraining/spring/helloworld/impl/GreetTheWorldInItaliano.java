package com.betse.betraining.spring.helloworld.impl;

import com.betse.betraining.spring.helloworld.*;

/**
 * Created by IntelliJ IDEA.
 * User: angelo
 * Date: 20/04/17
 * Time: 10.32
 * SVN Id: $Id:$
 * .
 */
public class GreetTheWorldInItaliano implements GreetTheWorld{
  public String sayHello() {
    return "Salve, mondo!";
  }
}
