package com.betse.betraining.spring.helloworld.impl;

import com.betse.betraining.spring.helloworld.*;

/**
 * Created by IntelliJ IDEA.
 * User: angelo
 * Date: 20/04/17
 * Time: 10.34
 * SVN Id: $Id:$
 * .
 */
public class GreetTheWorldPoPolsku implements GreetTheWorld {
  public String sayHello() {
    return "Dzień dobry, świecie!";
  }
}
